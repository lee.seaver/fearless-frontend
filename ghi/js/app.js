function createCard(
  name,
  description,
  pictureUrl,
  newStart,
  newEnd,
  locationName
) {
  return `
      <div class="card shadow-lg p-3 mb-5 bg-body rounded">
    
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
            <div class="card-footer text-muted">
                ${newStart} - ${newEnd}
            </div>  
        </div>
      </div>
   

    `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";
  const columns = document.querySelectorAll(".col");
  let colIndx = 0;

  try {
    const response = await fetch(url);

    if (!response.ok) {
      return `<div class="alert alert-primary" role="alert">
            A simple primary alert—check it out!
             </div>`;
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          console.log(details);
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const locationName = details.conference.location.name;

          const startDate = new Date(details.conference.starts);
          const newStart = startDate.toLocaleDateString();

          const endDate = new Date(details.conference.ends);
          const newEnd = endDate.toLocaleDateString();

          const html = createCard(
            name,
            description,
            pictureUrl,
            newStart,
            newEnd,
            locationName
          );
          const column = columns[colIndx];
          column.innerHTML += html;
          colIndx = (colIndx + 1) % 3;
        }
      }
    }
  } catch (e) {
    console.error("error", error);
  }
});
