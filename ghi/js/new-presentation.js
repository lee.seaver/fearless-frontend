window.addEventListener("DOMContentLoaded", async () => {
    const conferenceUrl = "http://localhost:8000/api/conferences/";
    try {
        const response = await fetch(conferenceUrl);
        console.log(response);
        if (response.ok) {
            const data = await response.json();

            const selectTag = document.getElementById("conference");
            for (let conference of data.conferences) {
                const option = document.createElement("option");
                option.value = conference.href;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
            }
            const formTag = document.getElementById("create-presentation-form");
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const conferenceId = selectTag.value;
                const json = JSON.stringify(Object.fromEntries(formData));
                const presentationUrl = `http://localhost:8000${conferenceId}presentations/`
                console.log(json)
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        "Content-Type": "application/json"
                    },
                };
                const response = await fetch(presentationUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newPresentation = await response.json();
                }
            });
        }
    } catch (e) {
        console.log(e);
    }
});

// const url = 'http://localhost:8000/api/conferences/1/presentations/'
