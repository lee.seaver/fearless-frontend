// Creating a new AttendeesList function component in there and exporting it as the "default"
// Moving the table JSX and its contents from App.js to the new function
// Importing the AttendeesList from the AttendeesList.js module
// Using the new AttendeesList function component in the App function component

function AttendeesList(props) {
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Conference</th>
            </tr>
          </thead>
          <tbody>
            {props.attendees.map(attendee => {
              return (
                <tr key={attendee.href}>
                  <td>{ attendee.name }</td>
                  <td>{ attendee.conference }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
    );
}

export default AttendeesList;