import React, { useEffect, useState } from 'react';

function ConferenceForm (props) {

    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');
    const [name, setName] = useState('')
    const [starts, setStartDate] = useState('')
    const [ends, setEndDate] = useState('')
    const [description, setDescription] = useState('')
    const [max_presentations, setMaxPresentations] = useState('')
    const [max_attendees, setMaxAttendees] = useState('')
    

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value);
    };
    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocation(value);
    };
    const handleStartDateChange = (event) => {
        const value = event.target.value
        setStartDate(value);
    };
    const handleEndDateChange = (event) => {
        const value = event.target.value
        setEndDate(value);
    };
    const handleDescriptionChange = (event) => {
        const value = event.target.value
        setDescription(value);
    };
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value
        setMaxPresentations(value);
    };
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value
        setMaxAttendees(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = max_presentations;
        data.max_attendees = max_attendees;
        data.location = location

        console.log(data);

        const conferenceUrl = "http://localhost:8000/api/conferences/"
        const fetchConfig ={
            method:"POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConferece = await response.json();
            console.log(newConferece);
            setDescription('');
            setEndDate('');
            setLocation('');
            setMaxAttendees('');
            setMaxPresentations('');
            setName('');
            setStartDate('');
        }
    };

    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/";

        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setLocations(data.locations);

        }
    }
    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Conference</h1>
              <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <label htmlFor="name">Name</label>
                  <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" value ={name} className="form-control" />
                </div>
                <div className="form-floating mb-3">
                  <label htmlFor="starts">Start Date</label>
                  <input onChange={handleStartDateChange} placeholder="Start Date" required type="date" name="starts" id="starts" value={starts} className="form-control" />
                </div>
                <div className="form-floating mb-3">
                  <label htmlFor="ends">End Date</label>
                  <input onChange={handleEndDateChange} placeholder="End Date" required type="date" name="ends" id="ends" value={ends} className="form-control" />
                </div>
                <div className="form-floating mb-3">
                  <label htmlFor="description">Description</label>
                  <textarea onChange={handleDescriptionChange} placeholder="Description" name="description" id="description" value={description} cols="30" rows="10" className="form-control" required></textarea>
                </div>
                <div className="form-floating mb-3">
                  <label htmlFor="max_presentations">Max Presentations</label>
                  <input onChange={handleMaxPresentationsChange} placeholder="Max Presentations" required type="number" name="max_presentations" value={max_presentations} id="max_presentations" className="form-control" />
                </div>
                <div className="form-floating mb-3">
                  <label htmlFor="max_max_attendees">Max Attendees</label>
                  <input onChange={handleMaxAttendeesChange} placeholder="Max Attendees" required type="number" name="max_attendees" value={max_attendees} id="max_attendees" className="form-control" />
                </div>
                <div className="mb-3">
                  <select onChange={handleLocationChange} required name="location" id="location" value={location} className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                        return (
                            <option value={location.id} key={location.id}>
                                {location.name}
                            </option>
                        );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;