import Nav from "./nav";
import React from "react";
import ConferenceForm from "./ConferenceForm";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import AttendConference from "./AttendConference";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage.js"
import { BrowserRouter, Routes, Route } from "react-router-dom";



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
    <Routes>
      <Route index element={<MainPage />} />
      <Route path="locations">
        <Route path="new" element={<LocationForm />} />
      </Route>
      <Route path="conferences">
        <Route path="new" element={<ConferenceForm />} />
      </Route>
      <Route path="attendees">
        <Route index element={<AttendeesList attendees={props.attendees} />} />
        <Route path="new" element={<AttendConference />} />
      </Route>
      <Route path="presentations">
        <Route path="new" element={<PresentationForm />} />
      </Route>
    </Routes>
  </BrowserRouter>
  );
}

export default App;
