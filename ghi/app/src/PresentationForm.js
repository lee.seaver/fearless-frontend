import React, { useEffect, useState } from 'react';

function PresentationForm(props) {

  const [conferences, setConferences] = useState([])
  const [conference, setConference] = useState('')
  const [presenter_name, setPresenterName] = useState('')
  const [company_name, setCompanyName] = useState('')
  const [presenter_email, setPresenterEmail] = useState('')
  const [title, setTitle] = useState('')
  const [synopsis, setSynopsis] = useState('')

  const handlePresenterNameChange = (event) => {
    const value = event.target.value
    setPresenterName(value);
  };
  const handleCompanyNameChange = (event) => {
    const value = event.target.value
    setCompanyName(value);
  };
  const handlePresenterEmailChange = (event) => {
    const value = event.target.value
    setPresenterEmail(value);
  };
  const handleTitleChange = (event) => {
    const value = event.target.value
    setTitle(value);
  };
  const handleSynopsisChange = (event) => {
    const value = event.target.value
    setSynopsis(value);
  };
  const handleConferenceChange = (event) => {
    const value = event.target.value
    setConference(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.presenter_name = presenter_name;
    data.company_name = company_name;
    data.presenter_email = presenter_email;
    data.title = title;
    data.synopsis = synopsis;

    const presentationUrl = `http://localhost:8000${conference}presentations/`
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      setCompanyName('');
      setConference('');
      setPresenterEmail('');
      setPresenterName('');
      setSynopsis('');
      setTitle('');

    }

  };


  const fetchData = async () => {
    const conferenceUrl = 'http://localhost:8000/api/conferences/'
    const response = await fetch(conferenceUrl);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences)
    }
  }
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <label htmlFor="presenter_name">Presenter Name</label>
                <input onChange={handlePresenterNameChange} placeholder="Presenter Name" required type="text" name="presenter_name" id="presenter_name" value={presenter_name} className="form-control" />
              </div>
              <div className="form-floating mb-3">
                <label htmlFor="company_name">Company Namel</label>
                <input onChange={handleCompanyNameChange} placeholder="Company Name" required type="text" name="company_name" id="company_name" value={company_name} className="form-control" />
              </div>
              <div className="form-floating mb-3">
                <label htmlFor="presenter_email">Presenter Email</label>
                <input onChange={handlePresenterEmailChange} placeholder="Presenter Email" required type="text" name="presenter_email" id="presenter_email" value={presenter_email} className="form-control" />
              </div>
              <div className="form-floating mb-3">
                <label htmlFor="title">Title</label>
                <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" value={title} className="form-control" />
              </div>
              <div className="form-group mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange} name="synopsis" id="synopsis" cols="30" rows="10" value={synopsis} className="form-control" required></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConferenceChange} required name="conference" id="conference" value={conference} className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                      <option value={conference.href} key={conference.href}>
                        {conference.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
export default PresentationForm;